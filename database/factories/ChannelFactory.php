<?php

use App\Models\User;
use Faker\Generator as Faker;

$factory->define(App\Models\Channel::class, function (Faker $faker) {

    $user_id = factory(User::class)->create()->id;
    $name = $faker->word;
    return [
        'name' => $name,
        'slug' => str_slug($name),
        'user_id' => $user_id,
        'description' => $faker->sentence
    ];
});
