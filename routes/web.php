<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/emailtest', function() {
    Mail::to('gawronski.p@gmail.com')
        ->send(new \App\Mail\Test());
});

Route::prefix('elasticsearch')->group(function(){
    Route::get('test', 'ClientController@testelastic');
    Route::get('data', 'ClientController@dataelastic');
    Route::get('queries', 'ClientController@queryelastic');
});

Route::prefix('elastica')->group(function(){
    Route::get('test', 'ClientController@testelastica');
    Route::get('data', 'ClientController@dataelastica');
    Route::get('queries', 'ClientController@queryelastica');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function() {
    Route::get('channel/{channel}/edit', 'ChannelSettingsController@edit')->name('channel.edit');
    Route::put('channel/{channel}/edit', 'ChannelSettingsController@update')->name('channel.update');

    Route::get('upload', 'VideoUploadController@index')->name('video.index');
});
