<?php

namespace Tests\Unit;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class UserUnitTest extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;

    /** @test */
    public function it_creates_a_person_and_persist_it_to_the_database()
    {
        $newUser = factory(User::class)->create();
        $user = User::findOrFail($newUser->id);

        $this->assertEquals($newUser->all(), $user->all());
    }
}
