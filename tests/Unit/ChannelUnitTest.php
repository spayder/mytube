<?php

namespace Tests\Unit;

use App\Models\Channel;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class ChannelUnitTest extends TestCase
{
    use DatabaseTransactions, DatabaseMigrations;

    /** @test */
    public function it_creates_and_persists_in_database_a_new_channel()
    {
        $newChannel = factory(Channel::class)->create();
        $channel = Channel::findOrFail($newChannel->id);

        $this->assertEquals($newChannel->all(), $channel->all());
    }
}
