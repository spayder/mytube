<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChannelUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255|unique:channels,name,' . $this->id,
            'slug' => 'required|alpha_dash|max:255|unique:channels,name,' . $this->id,
            'description' => 'max:1000',
            'image'=> 'image'
        ];
    }
}
