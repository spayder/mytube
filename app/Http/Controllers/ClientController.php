<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Elastica\Client as ElasticaClient;
use Elastica\Document;
use Elastica\Type\Mapping;
use Elasticsearch\ClientBuilder;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    protected $elasticsearch;
    protected $elasticaIndex;

    public function __construct()
    {
        $this->elasticsearch = ClientBuilder::create()
            ->setHosts(['elasticsearch'])
            ->build();
        
        $config = [
            'host' => 'elasticsearch',
            'port' => 9200
        ];
        $elastica = new ElasticaClient($config);
        $this->elasticaIndex = $elastica->getIndex('pets');
    }

    public function testelastic()
    {
        $params = [
            'index' => 'pets',
            'type' => 'dog',
            'id' => 1
        ];

        $response = $this->elasticsearch->get($params);
        dump($response);
    }

    public function testelastica()
    {
        $dogType = $this->elasticaIndex->getType('dog');

        $response = $dogType->getDocument(1);

        dd($response);
    }

    public function dataelastic()
    {
        // set a mapping for a document
//        $params = [
//            'index' => 'pets',
//            'type' => 'birds',
//            'body' => [
//                'birds' => [
//                    '_source' => [
//                        'enabled' => true
//                    ],
//                    'properties' => [
//                        'name' => ['type' => 'string'],
//                        'age' => ['type' => 'long'],
//                        'gender' => ['type' => 'string'],
//                        'color' => ['type' => 'string'],
//                        'braveBird' => ['type' => 'boolean'],
//                        'hometown' => ['type' => 'string'],
//                        'about' => ['type' => 'text'],
//                        'registered' => ['type' => 'date']
//                    ]
//                ]
//            ]
//        ];
//
//        $response = $this->elasticsearch->indices()->putMapping($params);
//        dd($response);

        // index a document
        $faker = \Faker\Factory::create();
//        $params = [
//            'index' => 'pets',
//            'type' => 'birds',
//            'id' => 1,
//            'body' => [
//                'name' => $faker->name,
//                'age' => rand(1, 10),
//                'gender' => array_rand(['male', 'female']),
//                'color' => $faker->colorName,
//                'braveBird' => rand(0, 1),
//                'hometown' => $faker->city,
//                'about' => $faker->sentence(),
//                'registered' => Carbon::now()->subWeeks(20)->format('Y-m-d')
//            ]
//        ];
//
//        $response = $this->elasticsearch->index($params);
//        dd($response);

        // index a bunch of documents
        $params = [];
        for($i = 0; $i <= 100; $i++) {
            $params['body'][] = [
                'index' => [
                    '_index' => 'pets',
                    '_type' => 'birds'
                ]
            ];

            $gender = $faker->randomElement(['male', 'female']);
            $age = $faker->numberBetween(1, 15);

            $params['body'][] = [
                'name' => $faker->name($gender),
                'age' => $age,
                'gender' => $gender,
                'color' => $faker->safeColorName,
                'braveBird' => $faker->boolean(),
                'hometown' => $faker->city . ', ' . $faker->state,
                'about' => $faker->realText(),
                'registered' => $faker->dateTimeBetween('- ' . $age . 'years', 'now')->format('Y-m-d')
            ];
        }

        $response = $this->elasticsearch->bulk($params);
        dd($response);
    }

    public function dataelastica()
    {
        $faker = \Faker\Factory::create();
        $catType = $this->elasticaIndex->getType('cat');
//

        // set a mapping for a new documents
//        $mapping = new Mapping($catType, [
//            'name' => ['type' => 'string'],
//            'age' => ['type' => 'long'],
//            'gender' => ['type' => 'string'],
//            'color' => ['type' => 'string'],
//            'prettyKitty' => ['type' => 'boolean'],
//            'hometown' => ['type' => 'string'],
//            'about' => ['type' => 'text'],
//            'registered' => ['type' => 'date']
//        ]);
//
//        $response = $mapping->send();
//        dd($response);

        // index a new document
//        $catDocument = new Document();
//        $catDocument->setData([
//            'name' => $faker->name,
//            'age' => rand(1, 10),
//            'gender' => array_rand(['male', 'female']),
//            'color' => $faker->colorName,
//            'prettyKitty' => rand(0, 1),
//            'hometown' => $faker->city,
//            'about' => $faker->sentence(),
//            'registered' => Carbon::now()->subWeeks(20)->format('Y-m-d')
//        ]);
//
//        $response = $catType->addDocument($catDocument);
//        dd($response);

        // index a bunch of new documents
        $documents = [];

        for($i=0; $i<=100; $i++) {
            $gender = $faker->randomElement(['male', 'female']);
            $age = $faker->numberBetween(1, 15);

            $documents[] = (new Document())->setData([
                'name' => $faker->name($gender),
                'age' => $age,
                'gender' => $gender,
                'color' => $faker->safeColorName,
                'prettyKitty' => $faker->boolean(),
                'hometown' => $faker->city . ', ' . $faker->state,
                'about' => $faker->realText(),
                'registered' => $faker->dateTimeBetween('- ' . $age . 'years', 'now')->format('Y-m-d')
            ]);
        }

        $response = $catType->addDocuments($documents);
        dd($response);
    }

    public function queryelastic()
    {
        $params = [
            'index' => 'pets',
            'type' => 'birds',
            'body' => [
                'query' => [
                    'match' => [
                        'name' => 'MD'
                    ]
                ]
            ]
        ];

        $response = $this->elasticsearch->search($params);
//        dump($response);

        $params = [
            'index' => 'pets',
            'type' => 'birds',
            'size' => 15,
            'body' => [
                'query' => [
                    'match' => [
                        'about' => 'Alice'
                    ]
                ]
            ]
        ];

        $response = $this->elasticsearch->search($params);
//        dump($response);

        $params = [
            'index' => 'pets',
            'type' => 'birds',
            'body' => [
                'query' => [
                    'bool' => [
                        'must' => [
                            'match' => ['about' => 'Alice'],
//                            'match' => ['age' => '11']
                        ],
                        'should' => [
                            'term' => ['gender' => 'male'],
                            'term' => ['braveBird' => true]
                        ],
                        'filter' => [
                            'range' => [
                                'registered' => [
                                    'lte' => '2015-01-01'
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $response = $this->elasticsearch->search($params);
        dump($response);
    }

    public function queryelastica()
    {
        $birdType = $this->elasticaIndex->getType('birds');

        $query = new \Elastica\Query;
        $match = new \Elastica\Query\Match('name', 'MD');
        $query->setQuery($match);

        $response = $birdType->search($query);
//        dump($response);

        $query = new \Elastica\Query;
        $match = new \Elastica\Query\Match;
        $match->setField('about', 'Alice');
        $query->setSize(15);
        $query->setQuery($match);

        $response = $birdType->search($query);
//        dump($response);

        $query = new \Elastica\Query;
        $bool = new \Elastica\Query\BoolQuery;
        $match1 = new \Elastica\Query\Match('about', 'Alice');
//        $match2 = new \Elastica\Query\Match('age', '11');
        $should1 = new \Elastica\Query\Term(['braveBird' => true]);
        $should2 = new \Elastica\Query\Term(['gender' => 'female']);
        $filter = new \Elastica\Query\Range('registered', ['gte' => '2015-01-01']);

        $bool->addMust($match1);
//        $bool->addMust($match2);
        $bool->addShould($should1);
        $bool->addShould($should2);
        $bool->addFilter($filter);

        $query->setQuery($bool);

        $response = $birdType->search($query);
        dump($response);
    }
}
